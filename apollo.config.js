module.exports = {
  client: {
    service: {
      localSchemaFile: __dirname + '/.schema/graphql.json',
    },
    includes: ['./api/**/*.ts'],
    outputFlat: 'types'
  }
};
