import Image from 'next/image';
import { ProductList_productList } from '../types/ProductList';
import { useContext, useEffect, useState } from 'react';
import { UserContext } from '../context/user-context';
import { useApollo } from '../api/apollo-client';
import { parseBadges, getHighestBadge, Badge } from '../lib/badge';
import { User_user, User_user_offers } from '../types/User';
import Loading from '../components/loading';

type BadgesProps = {
  product: ProductList_productList;
};

const getApplicableOffers = (product: ProductList_productList, user: User_user): User_user_offers[] => {
  const offerIds = new Set(product.offer_ids || []);
  const userOffers = user.offers || [];
  return userOffers.filter((offer) => offerIds.has(offer.id));
};

const Badges = ({ product }: BadgesProps) => {
  const context = useContext(UserContext);
  const client = useApollo(null);
  const [badge, setBadge] = useState<Badge>(null);
  const [loading, setIsLoading] = useState(false);
  useEffect(() => {
    if (!context.user.auth) {
      setBadge(null);
      setIsLoading(false);
      return;
    }
    setBadge(null);
    setIsLoading(true);
    const promise = context.user.meta ? Promise.resolve(context.user) : context.fetchInfo(client);
    promise.then((user) => {
      const badges = parseBadges(user.meta.available_badges);
      const offers = getApplicableOffers(product, user.meta);
      setBadge(getHighestBadge(badges, offers));
      setIsLoading(false);
    });
  }, [context.user.userId]);

  const imageLoader = ({ src }) => {
    return `assets/img/${src}_icon.jpg`;
  };
  return (
    <>
      {badge ? (
        <div>
          <Image src={badge.name} alt={badge.name} loader={imageLoader} width={160} height={60} />
        </div>
      ) : loading ? (
        <Loading />
      ) : (
        ''
      )}
    </>
  );
};

export default Badges;
