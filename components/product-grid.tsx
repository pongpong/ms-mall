import { useQuery } from '@apollo/client';
import { Row } from 'react-bootstrap';
import { ALL_PRODUCTS_QUERY } from '../api/product-api';
import { ProductList_productList } from '../types/ProductList';
import ErrorMessage from './error-msg';
import styles from '../styles/ProductGrid.module.css';
import Link from 'next/link';
import Image from 'next/image';
import Loading from '../components/loading';
import { imageLoader } from '../lib/loader';
import ProductPrice from '../components/product-price';
import Badges from '../components/badges';

type ProductInfoProps = {
  product: ProductList_productList;
};

const ProductInfo = ({ product }: ProductInfoProps) => {
  return (
    <div key={product.id} className="col-md-4 col-sm-6">
      <div className={styles['product-grid']}>
        <div className={styles['product-img']}>
          <Link as={`/products/${product.id}`} href="/products/[pid]">
            <Image
              loader={imageLoader}
              className={styles['pic-1']}
              alt={product.name}
              src={product.image_key}
              width={308}
              height={400}
            />
          </Link>
        </div>
        <div className={styles['product-content']}>
          <h3 className={styles.title}>
            <Link as={`/products/${product.id}`} href="/products/[pid]">
              <a title={product.name}>{product.name}</a>
            </Link>
          </h3>
          <ProductPrice price={product.price} />
          <Badges product={product} />
        </div>
      </div>
    </div>
  );
};

export default function ProductGrid() {
  const { loading, error, data, fetchMore, networkStatus } = useQuery(ALL_PRODUCTS_QUERY, {
    // Setting this value to true will make the component rerender when
    // the "networkStatus" changes, so we are able to know if it is fetching
    // more data
    notifyOnNetworkStatusChange: true,
  });

  if (error) return <ErrorMessage message="Error loading products." />;
  if (loading) return <Loading />;
  return (
    <>
      <Row>
        {data.productList.map((product: ProductList_productList, index: number) => (
          <ProductInfo product={product} />
        ))}
      </Row>
    </>
  );
}
