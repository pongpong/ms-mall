import { ProductDetails_product_price } from '../types/ProductDetails';

type ProductPriceProps = {
  price: ProductDetails_product_price;
};
const ProductPrice = ({ price }: ProductPriceProps) => {
  return (
    <div className="price">
      {price.original_price ? (
        <span className="mr-3">
          <s>
            {price.currency_code} {price.original_price}
          </s>
        </span>
      ) : (
        ''
      )}
      <span>
        {price.currency_code} {price.current_price}
      </span>
    </div>
  );
};

export default ProductPrice;
