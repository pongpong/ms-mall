import { UserContext } from '../context/user-context';
import React, { useContext } from 'react';

const UserSwitcher = () => {
  const context = useContext(UserContext);
  const handleClick = (userId: number) => {
    context.logout();
    context.login(userId + '');
  };
  return (
    <div>
      <span className="mr-3">User: {context.user.auth ? context.user.userId : ''}</span>
      {[...Array(5)].map((x, i) => (
        <span key={'user' + i}>
          <button onClick={() => handleClick(i + 1)}>{i + 1}</button>
        </span>
      ))}
      <span>
        <button onClick={() => context.logout()}>Logout</button>
      </span>
    </div>
  );
};

export default UserSwitcher;
