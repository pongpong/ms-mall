import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import ProductPrice from '../components/product-price';
import { ProductDetails_product } from '../types/ProductDetails';
import { imageLoader } from '../lib/loader';
import { Button } from 'react-bootstrap';

const BackButton = () => {
  const router = useRouter();
  return <Button onClick={() => router.back()}>Back</Button>;
};

const ProductImage = ({ product }: ProductDetailsProps) => {
  return (
    <div className="h-100 text-center">
      <Image loader={imageLoader} src={product.image_key} alt={product.name} width="308" height="400" />
    </div>
  );
};

const ProductCard = ({ product }: ProductDetailsProps) => {
  return (
    <div className="card-group">
      <div className="card">
        <div className="card-body">
          <h2 className="card-title">{product.name}</h2>
          <ProductPrice price={product.price} />
          <hr />
          <p className="card-text">
            {product.information ? <h5 className="card-text">{product.information[0].section_text}</h5> : ''}
          </p>
        </div>
      </div>
    </div>
  );
};

type ProductDetailsProps = {
  product: ProductDetails_product;
};

export default function ProductDetails({ product }: ProductDetailsProps) {
  return (
    <div className="col-md-12 mb-30px">
      <div className="h-100">
        <BackButton />
        <div className="row h-100">
          <div className="col-12 col-lg-4 pr-lg-0">
            <ProductImage product={product} />
          </div>
          <div className="col-12 col-md-12 col-lg-7">
            <ProductCard product={product} />
          </div>
        </div>
      </div>
    </div>
  );
}
