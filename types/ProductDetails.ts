/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ProductDetails
// ====================================================

export interface ProductDetails_product_price {
  __typename: 'Price';
  currency_code: string;
  current_price: number;
  original_price: number | null;
}

export interface ProductDetails_product_information {
  __typename: 'ProductInformation';
  section_text: string | null;
  section_title: string | null;
}

export interface ProductDetails_product {
  __typename: 'Product';
  name: string;
  image_key: string;
  price: ProductDetails_product_price;
  information: (ProductDetails_product_information | null)[] | null;
}

export interface ProductDetails {
  product: ProductDetails_product | null;
}

export interface ProductDetailsVariables {
  pid: string;
}
