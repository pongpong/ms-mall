/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ProductList
// ====================================================

export interface ProductList_productList_price {
  __typename: 'Price';
  currency_code: string;
  current_price: number;
  original_price: number | null;
}

export interface ProductList_productList {
  __typename: 'Product';
  id: string;
  name: string;
  image_key: string;
  offer_ids: string[] | null;
  price: ProductList_productList_price;
}

export interface ProductList {
  productList: ProductList_productList[];
}
