/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OfferType } from './globalTypes';

// ====================================================
// GraphQL query operation: User
// ====================================================

export interface User_user_offers {
  __typename: 'Offer';
  id: string;
  title: string;
  type: OfferType;
}

export interface User_user {
  __typename: 'User';
  id: string;
  available_badges: string;
  offers: User_user_offers[] | null;
}

export interface User {
  user: User_user | null;
}

export interface UserVariables {
  uid: string;
}
