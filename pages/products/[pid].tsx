import { useQuery } from '@apollo/client';
import React from 'react';
import Loading from '../../components/loading';
import ProductDetails from '../../components/product-details';
import ErrorMessage from '../../components/error-msg';
import { PRODUCT_DETAILS_QUERY } from '../../api/product-api';
import { useRouter } from 'next/router';

export default function Product() {
  const router = useRouter();
  const { pid } = router.query;
  const { loading, error, data, fetchMore, networkStatus } = useQuery(PRODUCT_DETAILS_QUERY, {
    variables: { pid: pid },
  });
  if (loading) return <Loading />;
  if (error) return <ErrorMessage message="error loading product" />;
  return <ProductDetails product={data.product} />;
}
