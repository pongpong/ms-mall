import Layout from '../components/layout';
import Container from '../components/container';
import Head from 'next/head';
import ProductGrid from '../components/product-grid';
import { initializeApollo, addApolloState } from '../api/apollo-client';
import UserSwitcher from '../components/user-switcher';
import { Row } from 'react-bootstrap';

const Index = () => {
  return (
    <>
      <Layout>
        <Head>
          <title>Product List</title>
        </Head>
        <Container>
          <Row>
            <UserSwitcher />
          </Row>
          <ProductGrid />
        </Container>
      </Layout>
    </>
  );
};

export default Index;

export const getStaticProps = async () => {
  const apolloClient = initializeApollo();

  return addApolloState(apolloClient, {
    props: {},
    revalidate: 1,
  });
};
