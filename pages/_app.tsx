import { AppProps } from 'next/app';
import '../styles/globals.css';
import { ApolloProvider } from '@apollo/client';
import { useApollo } from '../api/apollo-client';
import '../styles/Home.module.css';
import { UserProvider } from '../context/user-context';

export default function MyApp({ Component, pageProps }: AppProps) {
  const apolloClient = useApollo(pageProps);
  return (
    <ApolloProvider client={apolloClient}>
      <UserProvider>
        <Component {...pageProps} />
      </UserProvider>
    </ApolloProvider>
  );
}
