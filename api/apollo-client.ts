import { useMemo } from 'react';
import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client';
import { GRAPHQL_API_URL } from '../constants/app';

export const APOLLO_STATE_PROP_NAME = '__APOLLO_STATE__';

function createApolloClient({ uri }): ApolloClient<any> {
  return new ApolloClient({
    ssrMode: true,
    link: new HttpLink({
      uri,
    }),
    cache: new InMemoryCache(),
  });
}

let apolloClient: ApolloClient<any>;
export function initializeApollo(initialState = null) {
  const url = process.env.GRAPHQL_API_URL ?? GRAPHQL_API_URL;
  const _apolloClient = apolloClient ?? createApolloClient({ uri: url });

  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract();

    // Restore the cache using the data passed from
    // getStaticProps/getServerSideProps combined with the existing cached data
    _apolloClient.cache.restore({ ...existingCache, ...initialState });
  }

  // For SSG and SSR always create a new Apollo Client
  if (typeof window === 'undefined') return _apolloClient;

  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient;
  return _apolloClient;
}

export function useApollo(initialState: any) {
  return useMemo(() => initializeApollo(initialState), [initialState]);
}

export function addApolloState(client: ApolloClient<any>, pageProps: any) {
  if (pageProps?.props) {
    pageProps.props[APOLLO_STATE_PROP_NAME] = client.cache.extract();
  }

  return pageProps;
}
