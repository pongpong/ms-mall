import { gql } from '@apollo/client';

export const USER_QUERY = gql`
  query User($uid: String!) {
    user(id: $uid) {
      id
      available_badges
      offers {
        id
        title
        type
      }
    }
  }
`;

export const ALL_PRODUCTS_QUERY = gql`
  query ProductList {
    productList {
      id
      name
      image_key
      offer_ids
      price {
        currency_code
        current_price
        original_price
      }
    }
  }
`;

export const PRODUCT_DETAILS_QUERY = gql`
  query ProductDetails($pid: String!) {
    product(id: $pid) {
      name
      image_key
      price {
        currency_code
        current_price
        original_price
      }
      information {
        section_text
        section_title
      }
    }
  }
`;
