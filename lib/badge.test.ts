import { OfferType } from '../types/globalTypes';
import { parseBadges, getBadges, getHighestBadge } from './badge';

test('should return single badge given string', () => {
  expect(parseBadges('sale:PRIORITY_ACCESS,REDUCED,BONUS')).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        name: 'sale',
        types: ['PRIORITY_ACCESS', 'REDUCED', 'BONUS'],
      }),
    ])
  );
});

test('should return multipe badges given string', () => {
  expect(parseBadges('loyalty:SLOTTED,BONUS||sale:PRIORITY_ACCESS,REDUCED')).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        name: 'loyalty',
        types: ['SLOTTED', 'BONUS'],
      }),
      expect.objectContaining({
        name: 'sale',
        types: ['PRIORITY_ACCESS', 'REDUCED'],
      }),
    ])
  );
});

test('should return empty badges given empty string', () => {
  expect(parseBadges('')).toEqual([]);
  expect(parseBadges('  ')).toEqual([]);
  expect(parseBadges(null)).toEqual([]);
  expect(parseBadges(undefined)).toEqual([]);
});

test('should return single badge given simple string', () => {
  expect(parseBadges('invalid')).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        name: 'invalid',
        types: [],
      }),
    ])
  );
});

test('should return single badge given applicable offers', () => {
  const res = getBadges(parseBadges('sale:PRIORITY_ACCESS,REDUCED,BONUS'), [
    {
      __typename: 'Offer',
      id: '1',
      title: 'Get it while it lasts!',
      type: OfferType.REDUCED,
    },
    {
      __typename: 'Offer',
      id: '2',
      title: 'Extra loyalty points!',
      type: OfferType.BONUS,
    },
    {
      __typename: 'Offer',
      id: '3',
      title: 'Priority Access!',
      type: OfferType.PRIORITY_ACCESS,
    },
    {
      __typename: 'Offer',
      id: '4',
      title: 'Discount available!',
      type: OfferType.SLOTTED,
    },
    {
      __typename: 'Offer',
      id: '5',
      title: 'Super sale!',
      type: OfferType.BONUS,
    },
  ]);
  expect(res).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        name: 'sale',
        types: ['PRIORITY_ACCESS', 'REDUCED', 'BONUS'],
      }),
    ])
  );
});

test('should return multiple badges given applicable offers', () => {
  const res = getBadges(parseBadges('loyalty:SLOTTED,BONUS||sale:PRIORITY_ACCESS,REDUCED'), [
    {
      __typename: 'Offer',
      id: '6',
      title: 'Reductions!',
      type: OfferType.REDUCED,
    },
    {
      __typename: 'Offer',
      id: '7',
      title: 'Special!',
      type: OfferType.BONUS,
    },
    {
      __typename: 'Offer',
      id: '3',
      title: 'Priority Access!',
      type: OfferType.PRIORITY_ACCESS,
    },
  ]);
  expect(res).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        name: 'loyalty',
        types: ['SLOTTED', 'BONUS'],
      }),
      expect.objectContaining({
        name: 'sale',
        types: ['PRIORITY_ACCESS', 'REDUCED'],
      }),
    ])
  );
});

test('should return highest badge given applicable offers', () => {
  const res = getHighestBadge(parseBadges('loyalty:SLOTTED,BONUS||sale:PRIORITY_ACCESS,REDUCED'), [
    {
      __typename: 'Offer',
      id: '6',
      title: 'Reductions!',
      type: OfferType.REDUCED,
    },
    {
      __typename: 'Offer',
      id: '7',
      title: 'Special!',
      type: OfferType.BONUS,
    },
    {
      __typename: 'Offer',
      id: '3',
      title: 'Priority Access!',
      type: OfferType.PRIORITY_ACCESS,
    },
  ]);
  expect(res).toEqual(
    expect.objectContaining({
      name: 'loyalty',
      types: ['SLOTTED', 'BONUS'],
    })
  );
});
