import { IMAGE_BASE_URL } from '../constants/app';

type ImageLoaderProps = {
  src: string;
};

export const imageLoader = ({ src }: ImageLoaderProps) => {
  return `${IMAGE_BASE_URL}/${src}`;
};
