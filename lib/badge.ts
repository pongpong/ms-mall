import { User_user_offers } from '../types/User';

const omitEmptyString = (arr: string[]) => {
  return arr.map((s) => s.trim()).filter((s) => s.length);
};

export type Badge = {
  name: string;
  types: string[];
};

export const parseBadges = (s: string): Badge[] => {
  if (!!!s) return [];
  return omitEmptyString(s.split('||')).map((badge) => {
    const [name, types] = omitEmptyString(badge.split(':'));
    return { name, types: types ? omitEmptyString(types.split(',')) : [] };
  });
};

export const getBadges = (availableBadges: Badge[], applicableOffers: User_user_offers[]): Badge[] => {
  return availableBadges.filter((badge) => {
    const types = new Set(badge.types);
    for (const offer of applicableOffers) {
      if (types.has(offer.type)) {
        return true;
      }
    }
    return false;
  });
};

export const getHighestBadge = (
  availableBadges: Badge[],
  applicableOffers: User_user_offers[]
): Badge | null => {
  const badges = getBadges(availableBadges, applicableOffers);
  return badges.length ? badges[0] : null;
};
