## Getting Started

Make sure 3001 port and 80 port are not occupied with other applications. 3001 port is used by Graphql Server. and 80 port is used by our web application

First, run the docker compose to bring up both graphql server and web application:

```bash
docker-compose up
```

Open [http://localhost](http://localhost) with your browser to see the result.

- You may clicks the UI at the top to switch between userId 1-5
