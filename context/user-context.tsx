import React from 'react';
import { User_user, User } from '../types/User';
import cookie from 'js-cookie';
import { ApolloClient } from '@apollo/client';
import { USER_QUERY } from '../api/product-api';

export type UserState = {
  userId: string;
  meta: User_user | null;
  auth: boolean;
};

export type UserProps = {
  user: UserState;
  login: (userId: string) => void;
  fetchInfo: (client: ApolloClient<any>) => Promise<UserState>;
  logout: () => void;
};

// @function  UserContext
export const UserContext = React.createContext<UserProps>(null);
// @function  UserProvider
// Create function to provide UserContext
export const UserProvider = ({ children }) => {
  const [user, setUser] = React.useState<UserState>({
    userId: '',
    meta: null,
    auth: false,
  });

  const login = (userId: string) => {
    cookie.set('userId', userId, { expires: 14 });
    setUser((user) => ({
      userId,
      meta: null,
      auth: true,
    }));
  };

  const fetchInfo = async (client: ApolloClient<any>) => {
    if (!user.auth) {
      return Promise.reject(new Error('user not logged in'));
    }
    const result = await client.query<User>({
      query: USER_QUERY,
      variables: { uid: user.userId },
    });
    const newUser = {
      ...user,
      meta: { ...result.data.user },
    };
    setUser(newUser);
    return newUser;
  };

  const logout = () => {
    setUser((user) => ({
      meta: null,
      userId: '',
      auth: false,
    }));
  };

  return <UserContext.Provider value={{ user, fetchInfo, login, logout }}>{children}</UserContext.Provider>;
};
